package com.veragi.empresa.asmobile;

/**
 * Created by egimenez on 01/04/2016.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.veragi.empresa.asmobile.domain.Novedad;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by venkataprasad on 02-01-2015.
 */
class CardViewDataAdapter extends RecyclerView.Adapter<CardViewDataAdapter.ViewHolder> {

    private static ArrayList<Novedad> dataSet;

    public CardViewDataAdapter(ArrayList<Novedad> os_versions) {

        dataSet = os_versions;
    }


    @Override
    public CardViewDataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
// create a new view
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.card_view, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewDataAdapter.ViewHolder viewHolder, int i) {

        Novedad fp = dataSet.get(i);

        viewHolder.titulo.setText(fp.getTitulo());
        viewHolder.introduccion.setText(Html.fromHtml(fp.getIntroduccion()));
       // viewHolder.iconView.setImageResource(22);


        //Load bitmap from internet
        if(!"sin_imagen".equals(fp.getImagen())) {
            try {
                URL onLineURL = new URL(fp.getImagen());
                new MyNetworkTask(viewHolder.iconView).execute(onLineURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }else{
            viewHolder.iconView.setImageResource(R.drawable.sinimg);
        }

        viewHolder.feed = fp;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titulo;
        public TextView introduccion;
        public ImageView iconView;


        public Novedad feed;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            titulo = (TextView) itemLayoutView
                    .findViewById(R.id.titulo);
            introduccion = (TextView) itemLayoutView
                    .findViewById(R.id.introduccion);
            iconView = (ImageView) itemLayoutView
                    .findViewById(R.id.iconId);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), SecondPage.class);
                    intent.putExtra("titulo", feed.getTitulo());
                    intent.putExtra("descripcion", feed.getDescripcion());
                    intent.putExtra("ID_USUARIO", feed.getIdUsuario());

                    v.getContext().startActivity(intent);

                }
            });


        }

    }



    private class MyNetworkTask extends AsyncTask<URL, Void, Bitmap>{

        ImageView tIV;

        public MyNetworkTask(ImageView iv){
            tIV = iv;
        }

        @Override
        protected Bitmap doInBackground(URL... urls) {

            Bitmap networkBitmap = null;

            URL networkUrl = urls[0]; //Load the first element
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inSampleSize = 2;
                // networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream());
                networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream(), null, options);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return networkBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            tIV.setImageBitmap(result);
        }

    }

}
