package com.veragi.empresa.asmobile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.veragi.empresa.asmobile.domain.LocalidadSpinner;
import com.veragi.empresa.asmobile.domain.PaisSpinner;
import com.veragi.empresa.asmobile.domain.Propiedad;
import com.veragi.empresa.asmobile.domain.ProvinciaSpinner;
import com.veragi.empresa.asmobile.domain.TipoPropiedadSpinner;
import com.veragi.empresa.asmobile.helper.MyNetworkTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FotosActivity extends AppCompatActivity {
    private static final int PERMISSIONS_GRANTED = 0;

    Propiedad feed = new Propiedad();
    ArrayList<PaisSpinner> paises;
    ArrayList<ProvinciaSpinner> provincias;
    ArrayList<LocalidadSpinner> localidades;
    ArrayList<TipoPropiedadSpinner> tipoPropiedades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        LoginActivity.activities.add(this);


        //Variables de anterior pantalla
        feed.setIdUsuario(getIntent().getStringExtra("ID_USUARIO"));
        feed.setAddress(getIntent().getStringExtra("address"));
        feed.setPostcode(getIntent().getStringExtra("postcode"));
        feed.setCity("" + getIntent().getIntExtra("city", 0));
        feed.setState("" + getIntent().getIntExtra("state", 0));
        feed.setCountry("" + getIntent().getIntExtra("country", 0));
        feed.setPrice(getIntent().getStringExtra("price"));
        feed.setPro_small_desc(getIntent().getStringExtra("pro_small_desc"));
        feed.setPro_full_desc(getIntent().getStringExtra("pro_full_desc"));
        feed.setPro_type("" + getIntent().getIntExtra("pro_type", 0));
        feed.setPro_name(getIntent().getStringExtra("pro_name"));
        feed.setId(getIntent().getIntExtra("idPublicacion", 0));
        if (getIntent().getStringExtra("imagen") != null) {
            feed.setImagen(getIntent().getStringExtra("imagen"));
        }

        if (getIntent().getStringExtra("imagen2") != null) {
            feed.setImagen2(getIntent().getStringExtra("imagen2"));
        }

        if (getIntent().getStringExtra("imagen3") != null) {
            feed.setImagen3(getIntent().getStringExtra("imagen3"));
        }

        paises = (ArrayList<PaisSpinner>) getIntent().getSerializableExtra("paises");
        provincias = (ArrayList<ProvinciaSpinner>) getIntent().getSerializableExtra("provincias");
        localidades = (ArrayList<LocalidadSpinner>) getIntent().getSerializableExtra("localidades");
        tipoPropiedades = (ArrayList<TipoPropiedadSpinner>) getIntent().getSerializableExtra("tipoPropiedades");



        imgPreview1 = (ImageView) findViewById(R.id.imgPreview1);
        imgPreview1.setOnClickListener(new HandleTomarFoto());

        imgPreview2 = (ImageView) findViewById(R.id.imgPreview2);
        imgPreview2.setOnClickListener(new HandleTomarFoto());

        imgPreview3 = (ImageView) findViewById(R.id.imgPreview3);
        imgPreview3.setOnClickListener(new HandleTomarFoto());



        try{
            String urlImg1 = getIntent().getStringExtra("imagen");
            if(urlImg1.contains("http:")) {
                URL onLineURL = new URL(urlImg1);
                new MyNetworkTask(imgPreview1).execute(onLineURL);

            }else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                // downsizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg1)); //BitmapFactory.decodeFile(urlImg1, options);
                int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                imgPreview1.setImageBitmap(scaled);
            }


            String urlImg2 = getIntent().getStringExtra("imagen2");
            if(urlImg2.contains("http:")) {
                URL onLineURL = new URL(urlImg2);
                new MyNetworkTask(imgPreview2).execute(onLineURL);

            }else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                // downsizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg1)); //BitmapFactory.decodeFile(urlImg1, options);
                int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                imgPreview2.setImageBitmap(scaled);
            }

            String urlImg3 = getIntent().getStringExtra("imagen3");
            if(urlImg3.contains("http:")) {
                URL onLineURL = new URL(urlImg3);
                new MyNetworkTask(imgPreview3).execute(onLineURL);

            }else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                // downsizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg1)); //BitmapFactory.decodeFile(urlImg1, options);
                int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                imgPreview3.setImageBitmap(scaled);
            }



        }catch(Exception ex){
            ex.printStackTrace();
        }



        //Volver
        findViewById(R.id.boton_volver).setOnClickListener(new HandleClickVolver());
        findViewById(R.id.boton_continuar).setOnClickListener(new HandleClickContinuar());

    }

    private void confirmCancelarDialog(final View arg0) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setMessage("Desea cancelar los cambios?")
                .setPositiveButton("Si",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        setAtributosIntent(arg0, "Cancelar");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private class HandleClickVolver implements View.OnClickListener {
        public void onClick(View arg0) {
            confirmCancelarDialog(arg0);

        }
    }


    private class HandleClickContinuar implements View.OnClickListener {
        public void onClick(View arg0) {
            setAtributosIntent(arg0, "Continuar");

        }
    }

    private void setAtributosIntent(View arg0, String boton){
        Intent intent = new Intent(arg0.getContext(), AvisoActivity.class);

        if("Continuar".equals(boton)) {
            if (pathImg1 != null && !pathImg1.equals(feed.getImagen()))
                intent.putExtra("imagen", pathImg1);
            else
                intent.putExtra("imagen", feed.getImagen());

            if (pathImg2 != null && !pathImg2.equals(feed.getImagen2()))
                intent.putExtra("imagen2", pathImg2);
            else
                intent.putExtra("imagen2", feed.getImagen2());

            if (pathImg3 != null && !pathImg3.equals(feed.getImagen3()))
                intent.putExtra("imagen3", pathImg3);
            else
                intent.putExtra("imagen3", feed.getImagen3());
        }else{
            intent.putExtra("imagen", feed.getImagen());
            intent.putExtra("imagen2", feed.getImagen2());
            intent.putExtra("imagen3", feed.getImagen3());
        }



        //Mantengo los anteriores
        intent.putExtra("titulo", feed.getPro_name());
        intent.putExtra("address", feed.getAddress());
        intent.putExtra("postcode", feed.getPostcode());
        intent.putExtra("city", feed.getCity());
        intent.putExtra("state", feed.getState());
        intent.putExtra("country", feed.getCountry());
        intent.putExtra("precio", feed.getPrice());
        intent.putExtra("pro_small_desc", feed.getPro_small_desc());
        intent.putExtra("pro_full_desc", feed.getPro_full_desc());
        intent.putExtra("pro_type", feed.getPro_type());
        intent.putExtra("idPublicacion", feed.getId());
        intent.putExtra("ID_USUARIO", feed.getIdUsuario());
        intent.putExtra("paises", paises);
        intent.putExtra("provincias", provincias);
        intent.putExtra("localidades", localidades);
        intent.putExtra("tipoPropiedades", tipoPropiedades);
        intent.putExtra("fromActivity", "fotosActivity");


        arg0.getContext().startActivity(intent);
        this.finish();
    }


    public void askPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                PERMISSIONS_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_GRANTED: {
                selectImage(imgPreview1);
            }
        }
    }
    /**---------------INICIO FOTO----------------------*/
    /*
 * Capturing Camera Image will lauch camera app requrest image capture
 */
    private class HandleTomarFoto implements View.OnClickListener {
        public void onClick(View arg0) {

            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                askPermissions();

            } else {
                // capture picture
                if(arg0 == imgPreview1){
                    selectImage(imgPreview1);
                }
                if(arg0 == imgPreview2){
                    selectImage(imgPreview2);
                }
                if(arg0 == imgPreview3){
                    selectImage(imgPreview3);
                }
            }
        }
    }


    private void selectImage(final ImageView v) {
        final CharSequence[] options = { "Tomar foto", "Galeria", "Eliminar", "Cancelar" };

        AlertDialog.Builder builder = new AlertDialog.Builder(FotosActivity.this);
        //builder.setTitle("A!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Tomar foto")) {
                    v.setImageBitmap(null);
                    captureImage();
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);*/
                } else if (options[item].equals("Galeria")) {
                    v.setImageBitmap(null);
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Eliminar")) {
                    v.setImageBitmap(null);


                    //Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    String pathImg1;
    String pathImg2;
    String pathImg3;
    private ImageView imgPreview1;
    private ImageView imgPreview2;
    private ImageView imgPreview3;


    private Uri fileUri; // file url to store image/video
    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "zonaduenio";

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private void captureImage() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");

    }
    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view

                 //   Bundle MBuddle = data.getExtras();
                   // int _idImage = MBuddle.getInt("_idImage");

                    previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "Captura cancelada", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Error al tomar foto", Toast.LENGTH_SHORT)
                        .show();
            }
        }else if(requestCode == 2){
            if(data!=null) {
                Uri selectedImage = data.getData();

                final Bitmap thumbnail;
                try {
                    thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                    imgPreview1 = (ImageView) findViewById(R.id.imgPreview1);
                    imgPreview2 = (ImageView) findViewById(R.id.imgPreview2);
                    imgPreview3 = (ImageView) findViewById(R.id.imgPreview3);

                    int nh = (int) ( thumbnail.getHeight() * (800.0 / thumbnail.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(thumbnail, 600, nh, true);

                    if (!hasImage(imgPreview1)) {
                        imgPreview1.setImageBitmap(scaled);
                        pathImg1 = selectedImage.toString();
                    } else if (!hasImage(imgPreview2)) {
                        imgPreview2.setImageBitmap(scaled);
                        pathImg2 = selectedImage.toString();
                    } else if ((!hasImage(imgPreview3))) {
                        imgPreview3.setImageBitmap(scaled);
                        pathImg3 = selectedImage.toString();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }



            }


        }

        /**else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
         if (resultCode == RESULT_OK) {
         // video successfully recorded
         // preview the recorded video
         previewVideo();
         } else if (resultCode == RESULT_CANCELED) {
         // user cancelled recording
         Toast.makeText(getApplicationContext(),
         "User cancelled video recording", Toast.LENGTH_SHORT)
         .show();
         } else {
         // failed to record video
         Toast.makeText(getApplicationContext(),
         "Sorry! Failed to record video", Toast.LENGTH_SHORT)
         .show();
         }
         }*/
    }

    /**
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {
            // hide video preview
            // videoPreview.setVisibility(View.GONE);

            //imgPreview1.setVisibility(View.VISIBLE);

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            //final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.toString(),
              //      options);

            final Bitmap bitmap  = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);


            imgPreview1 = (ImageView) findViewById(R.id.imgPreview1);
            imgPreview2 = (ImageView) findViewById(R.id.imgPreview2);
            imgPreview3 = (ImageView) findViewById(R.id.imgPreview3);

            int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

            if(!hasImage(imgPreview1)){
                imgPreview1.setImageBitmap(scaled);
                pathImg1 = fileUri.toString();
            }else if(!hasImage(imgPreview2)){
                imgPreview2.setImageBitmap(scaled);
                pathImg2 = fileUri.toString();
            }else if((!hasImage(imgPreview3))){
                imgPreview3.setImageBitmap(scaled);
                pathImg3 = fileUri.toString();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**----------------FIN FOTO-------------------------*/

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }


        return hasImage;
    }


}

