package com.veragi.empresa.asmobile.provider;

import android.content.Context;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.veragi.empresa.asmobile.R;
import com.veragi.empresa.asmobile.domain.LocalidadSpinner;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gastonmazzeo on 5/29/18.
 */

public class LocalidadesProvider {

    public static List<LocalidadSpinner> localidades = new ArrayList<LocalidadSpinner>();

    public static void initialize(Context context) {
        try {

            InputStreamReader isReader = new InputStreamReader(context.getResources().openRawResource(R.raw.localidades));
            CSVReader reader = new CSVReader(isReader, ';');
            String [] nextLine;

            localidades.clear();
            while ((nextLine = reader.readNext()) != null) {
                //LOCALIDAD, LOCALIDAD_ID, PAIS_ID, PROVINCIA_ID
                localidades.add(new LocalidadSpinner(nextLine[1].toString(), Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[2]), Integer.parseInt(nextLine[3])));
            }

            Collections.sort(localidades, new Comparator<LocalidadSpinner>() {
                @Override
                public int compare(LocalidadSpinner o1, LocalidadSpinner o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "No se pudieron cargar las localidades", Toast.LENGTH_SHORT).show();
        }
    }

}
