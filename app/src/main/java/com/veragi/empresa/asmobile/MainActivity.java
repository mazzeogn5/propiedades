package com.veragi.empresa.asmobile;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.veragi.empresa.asmobile.domain.FeedProperties;
import com.veragi.empresa.asmobile.domain.Novedad;
import com.veragi.empresa.asmobile.helper.Constantes;
import com.veragi.empresa.asmobile.helper.CustomJSONObjectArrayRequest;
import com.veragi.empresa.asmobile.helper.CustomVolleyRequestQueue;
import com.veragi.empresa.asmobile.provider.PaisesProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends DrawerActivity implements Response.Listener,
        Response.ErrorListener  {

    public static final String REQUEST_TAG = "MainActivity";

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private CardView cardView;
    private RequestQueue mQueue;
    private ArrayList<FeedProperties> os_versions;
    private RecyclerView.Adapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LoginActivity.activities.add(this);
        idUsuario = (String)(getIntent().getStringExtra("ID_USUARIO"))!=null? Integer.parseInt((String)(getIntent().getStringExtra("ID_USUARIO"))) : 0;
        setContentView(R.layout.activity_main);
        this.initDrawer(2);
        mActivityTitle = getTitle().toString();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initContrls();

        // Init Providers
        PaisesProvider.initialize(this.getApplicationContext());

    }


    private void initContrls() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  cardView = (CardView) findViewById(R.id.cardList);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        if (toolbar != null) {
         //  setSupportActionBar(toolbar);
          //getSupportActionBar().setTitle("Android Versions");

        }


        recyclerView.setHasFixedSize(true);

        // ListView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Grid View
        // recyclerView.setLayoutManager(new GridLayoutManager(this,2,1,false));

        //StaggeredGridView
        // recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,1));

        // create an Object for Adapter


        //

        //




    }

    int intentosErrorNovedades = 0;
    private void cargarNovedades(){
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
        String url = Constantes.dominio + "novedades";
        Log.d("URL NOVEDADES", url);
        final CustomJSONObjectArrayRequest jsonRequest = new CustomJSONObjectArrayRequest(Request.Method
                .GET, url,
                new JSONObject(), this, this);
        jsonRequest.setTag(REQUEST_TAG);
        mQueue.add(jsonRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.hideError();
        this.showLoadingDialog("Cargando novedades.");
        cargarNovedades();
    }



    @Override
    protected void onStop() {
        super.onStop();
        this.hideLoadingDialog();
        if (mQueue != null) {
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(intentosErrorNovedades< Constantes.reintentos_novedades){
            cargarNovedades();
        }else{
            this.hideLoadingDialog();
            this.showError();
        }

        intentosErrorNovedades++;

    }

    @Override
    public void onResponse(Object response) {
        this.hideLoadingDialog();
        ArrayList<Novedad> novedades = new ArrayList<Novedad>();
        try {
            JSONArray ja = (JSONArray)response;

            for(int i = 0; i < ((JSONArray)response).length(); i++){
                JSONObject jResponse = ja.getJSONObject(i);

                Novedad nov = new Novedad();
                nov.setId(jResponse.getInt("id"));
                nov.setTitulo(jResponse.getString("title"));
                nov.setIntroduccion(jResponse.getString("introtext"));
                nov.setDescripcion(jResponse.getString("fulltext"));
                nov.setIdUsuario(idUsuario);
                if(nov.getDescripcion()!=null && !"".equals(nov.getDescripcion()))
                    nov.setDescripcion(nov.getDescripcion().replaceAll("src=\"images","http://www.xn--zonadueo-j3a.com/images" ));

                String imagenes = jResponse.getString("images");

                JSONObject obj = new JSONObject(imagenes);

                String pathImagen = "";
                try {
                    pathImagen = obj.getString("image_intro");
                } catch (Exception e){

                } finally {
                    String pathImagenOk = "";
                    if(pathImagen!=null && "".equals(pathImagen.trim())){
                        pathImagenOk = "sin_imagen";
                    }else if(pathImagen.contains("www")){
                        pathImagenOk = pathImagen;
                    }else{
                        pathImagenOk="http://www.xn--zonadueo-j3a.com/"+pathImagen;
                    }

                    nov.setImagen(pathImagenOk);
                }

                novedades.add(nov);
           }

        } catch (JSONException e) {
            this.hideLoadingDialog();
            e.printStackTrace();
        }


        mAdapter = new CardViewDataAdapter(novedades);
        recyclerView.setAdapter(mAdapter);

    }







}