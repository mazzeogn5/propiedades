package com.veragi.empresa.asmobile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by gastonmazzeo on 8/7/18.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showLoadingDialog() {
        this.showLoadingDialog("Cargando.");
    }

    public void showLoadingDialog(String message) {
        if (mDialog == null || (mDialog != null && !mDialog.isShowing())) {
            mDialog = new ProgressDialog(this);
            mDialog.setMessage(message);
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    public void hideLoadingDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public void showError() {
        LinearLayout containerError = (LinearLayout)findViewById(R.id.container_error);
        LinearLayout container = (LinearLayout)findViewById(R.id.container);

        containerError.setVisibility(View.VISIBLE);
        container.setVisibility(View.INVISIBLE);
    }

    public void hideError() {
        LinearLayout containerError = (LinearLayout)findViewById(R.id.container_error);
        LinearLayout container = (LinearLayout)findViewById(R.id.container);

        containerError.setVisibility(View.INVISIBLE);
        container.setVisibility(View.VISIBLE);
    }
}
