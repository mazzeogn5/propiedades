package com.veragi.empresa.asmobile.domain;

import com.veragi.empresa.asmobile.helper.Spinner;

import java.io.Serializable;

/**
 * Created by egimenez on 24/09/2016.
 */
public class ProvinciaSpinner extends Spinner implements Serializable {
    public String string;
    public Object tag;
    public int paisId;

    public ProvinciaSpinner(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    public ProvinciaSpinner(String string, Object tag, int paisId) {
        this.string = string;
        this.tag = tag;
        this.paisId = paisId;
    }

    @Override
    public String toString() {
        return string;
    }
}
