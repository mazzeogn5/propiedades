package com.veragi.empresa.asmobile.provider;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Environment;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.veragi.empresa.asmobile.R;
import com.veragi.empresa.asmobile.domain.PaisSpinner;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gastonmazzeo on 5/29/18.
 */

public class PaisesProvider {
    public static List<PaisSpinner> paises = new ArrayList<PaisSpinner>();

    public static void initialize(Context context) {
        try {
            InputStreamReader isReader = new InputStreamReader(context.getResources().openRawResource(R.raw.paises));
            CSVReader reader = new CSVReader(isReader, ';');
            String [] nextLine;

            paises.clear();
            while ((nextLine = reader.readNext()) != null) {
                paises.add(new PaisSpinner(nextLine[1].toString(), Integer.parseInt(nextLine[0])));
            }

            Collections.sort(paises, new Comparator<PaisSpinner>() {
                @Override
                public int compare(PaisSpinner o1, PaisSpinner o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "No se pudieron cargar los paises", Toast.LENGTH_SHORT).show();
        }
    }
}
