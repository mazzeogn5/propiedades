package com.veragi.empresa.asmobile.domain;

/**
 * Created by egimenez on 21/07/2016.
 */
public class SolucionSpinner {
    public String string;
    public Object tag;

    public SolucionSpinner(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}
