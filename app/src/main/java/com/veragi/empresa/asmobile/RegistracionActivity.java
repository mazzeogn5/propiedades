package com.veragi.empresa.asmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.android.volley.toolbox.StringRequest;
import com.veragi.empresa.asmobile.helper.Constantes;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistracionActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracion);
        LoginActivity.activities.add(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.boton_cancelar).setOnClickListener(new HandleCancelar());
        findViewById(R.id.boton_aceptar).setOnClickListener(new HandleAceptar());

    }

    private class HandleCancelar implements View.OnClickListener {
        public void onClick(View arg0) {
            Intent intent = new Intent(arg0.getContext(), LoginActivity.class);
            arg0.getContext().startActivity(intent);

        }
    }

    private class HandleAceptar implements View.OnClickListener {
        public void onClick (View arg0){
        final View argf = arg0;

            EditText nombre = (EditText) findViewById(R.id.campo_nombre);
            EditText correo = (EditText) findViewById(R.id.campo_correo);
            EditText pass = (EditText) findViewById(R.id.campo_pass);
            EditText rPass = (EditText) findViewById(R.id.campo_rpass);
            EditText telefono = (EditText) findViewById(R.id.campo_telefono);
            EditText celular = (EditText) findViewById(R.id.campo_celular);
            EditText direccion = (EditText) findViewById(R.id.campo_direccion);

            if(nombre.getText().toString().trim().length() < 1) {
                nombre.setError("El campo es obligatorio");
            }else if(correo.getText().toString().trim().length() < 1) {
                correo.setError("El campo es obligatorio");
            }else if(!isEmailValid(correo.getText().toString().trim())) {
                correo.setError("El formato ingresado no es correcto");
            }else if(pass.getText().toString().trim().length() < 1) {
                pass.setError("El campo es obligatorio");
            }else if(rPass.getText().toString().trim().length() < 1) {
                rPass.setError("El campo es obligatorio");
            }else if(pass.getText().toString().trim().length() < 8 || pass.getText().toString().trim().length() > 25) {
                pass.setError("El campo debe contener entre 8 y 25 caracteres");
            }else if(rPass.getText().toString().trim().length() < 8 || rPass.getText().toString().trim().length() > 25) {
                rPass.setError("El campo debe contener entre 8 y 25 caracteres");
            }else if(isPassValid(pass.getText().toString().trim())) {
                pass.setError("Debe tener entre 8 y 25 dígitos. Alfanumérico y respetando las minúsculas y minúsculas");
            }else if(isPassValid(rPass.getText().toString().trim())) {
                rPass.setError("Debe tener entre 8 y 25 dígitos. Alfanumérico y respetando las minúsculas y minúsculas");
            }else if(!pass.getText().toString().trim().equals(rPass.getText().toString().trim())) {
                pass.setError("Las contraseñas deben coincidir");
                rPass.setError("Las contraseñas deben coincidir");
            }else if(telefono.getText().toString().trim().length() < 1) {
                telefono.setError("El campo es obligatorio");
            }else if(celular.getText().toString().trim().length() < 1) {
                celular.setError("El campo es obligatorio");
            }else if(direccion.toString().trim().length() < 1) {
                direccion.setError("El campo es obligatorio");
            }else{
                //Crear usuario
                //mPostCommentResponse.requestStarted();
                showLoadingDialog("Registrando usuario.");
                StringRequest sr = new StringRequest(Request.Method.POST, Constantes.dominio + "users", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideLoadingDialog();
                        Toast.makeText(RegistracionActivity.this, "Usuario creado", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(argf.getContext(), LoginActivity.class);
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoadingDialog();
                        Toast.makeText(RegistracionActivity.this, "Ocurrio un error al crear el usuario" , Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("name", ((EditText) findViewById(R.id.campo_nombre)).getText().toString());
                        params.put("email", ((EditText) findViewById(R.id.campo_correo)).getText().toString());
                        params.put("password", ((EditText) findViewById(R.id.campo_pass)).getText().toString());
                        params.put("phone", ((EditText) findViewById(R.id.campo_rpass)).getText().toString());
                        params.put("mobile", ((EditText) findViewById(R.id.campo_celular)).getText().toString());
                        params.put("address", ((EditText) findViewById(R.id.campo_direccion)).getText().toString());

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(arg0.getContext());
                queue.add(sr);

            }
        }
    }

    public boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isPassValid(String pass) {
        Pattern pattern;
        Matcher matcher;
        String PASS_PATTERN = "/[^A-Za-z0-9]+/";
        pattern = Pattern.compile(PASS_PATTERN);
        matcher = pattern.matcher(pass);
        return matcher.matches();
    }


}
