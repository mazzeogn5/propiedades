package com.veragi.empresa.asmobile.helper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import android.app.ProgressDialog;
import android.widget.ProgressBar;
public class UploadToFtp {
    public FTPClient mFTPClient = null;
    public boolean ftpUpload1(String srcFilePath,String desFileName,String desDirectory)
    {
        String host="hostname";
        String username="username";
        String password="password";
        int port=21;
        mFTPClient = new FTPClient();
        boolean status = false;
        try {

            mFTPClient.connect(host, port);  // connecting to the host
            mFTPClient.login(username, password);  //Authenticate using username and password
            mFTPClient.changeWorkingDirectory(desDirectory); //change directory to that directory where image will be uploaded

            mFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
            BufferedInputStream buffIn = null;
            File file=new File(srcFilePath+desFileName);
            buffIn = new BufferedInputStream(new FileInputStream(file),8192);
            mFTPClient.enterLocalPassiveMode();
            status = mFTPClient.storeFile(desFileName, buffIn);

            buffIn.close();
            mFTPClient.logout();
            mFTPClient.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }
}