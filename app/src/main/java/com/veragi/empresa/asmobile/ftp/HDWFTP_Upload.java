package com.veragi.empresa.asmobile.ftp;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.veragi.empresa.asmobile.helper.Constantes;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by egimenez on 01/11/2016.
 */
public class HDWFTP_Upload extends AsyncTask<String, Void, Long> {

    private WeakReference<Context> contextRef;


    public HDWFTP_Upload(Context context) {
        contextRef = new WeakReference<>(context);

    }

    protected Long doInBackground(String... params) {

        {


            FTPClient ftpClient = new FTPClient();
            try {
                ftpClient.connect(Constantes.host, 21);
                if (FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                    boolean status = ftpClient.login(Constantes.user, Constantes.pass);

                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    ftpClient.enterLocalPassiveMode();

                    String srcFilePath = params[0];
                    String desFileName = params[1];
                    String directory_path = params[2];


                    if(status) {
                        //Context context = contextRef.get();

                        //InputStream srcFileStream = context.getContentResolver().openInputStream(Uri.parse(srcFilePath));
                        FileInputStream srcFileStream = new FileInputStream(srcFilePath);

                       if (ftpClient.changeWorkingDirectory(directory_path)) {
                           status = ftpClient.storeFile(desFileName, srcFileStream);
                           Log.d("FTP STATUS", "Status "+status);
                       }

                       srcFileStream.close();
                   }

                }

                ftpClient.logout();
                ftpClient.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        }


    }
}