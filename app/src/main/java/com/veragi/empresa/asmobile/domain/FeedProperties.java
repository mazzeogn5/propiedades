package com.veragi.empresa.asmobile.domain;

/**
 * Created by egimenez on 01/04/2016.
 */
/**
 * Created by venkataprasad on 02-01-2015.
 */
public class FeedProperties {


    private String title;
    private int thumbnail;
    private String nroTicket;
    private String comercio;
    private String telefono;
    private String domicilio;
    private String localidad;
    private String nroTerminal;
    private String provincia;
    private String cp;
    private String horarioVisita;
    private String tipoServicio;
    private String problemaReportado;
    private String observaciones;
    private String nroParte;
    private String solucion;
    private String cantInsumos;
    private String firmante;
    private String serieTerminal;
    private String serieImpresora;
    private String seriePinpad;
    private String host;
    private String moden;
    private String nroChip;
    private String nroBateria;
    private String empresa;
    private String estado;
    private int idPedido;
    private int idTecnico;
    private String descTecnico;
    private String legajo;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getComercio() {
        return comercio;
    }

    public void setComercio(String comercio) {
        this.comercio = comercio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getHorarioVisita() {
        return horarioVisita;
    }

    public void setHorarioVisita(String horarioVisita) {
        this.horarioVisita = horarioVisita;
    }

    public String getNroParte() {
        return nroParte;
    }

    public void setNroParte(String nroParte) {
        this.nroParte = nroParte;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    public String getCantInsumos() {
        return cantInsumos;
    }

    public void setCantInsumos(String cantInsumos) {
        this.cantInsumos = cantInsumos;
    }

    public String getFirmante() {
        return firmante;
    }

    public void setFirmante(String firmante) {
        this.firmante = firmante;
    }

    public String getSerieTerminal() {
        return serieTerminal;
    }

    public void setSerieTerminal(String serieTerminal) {
        this.serieTerminal = serieTerminal;
    }

    public String getSerieImpresora() {
        return serieImpresora;
    }

    public void setSerieImpresora(String serieImpresora) {
        this.serieImpresora = serieImpresora;
    }

    public String getSeriePinpad() {
        return seriePinpad;
    }

    public void setSeriePinpad(String seriePinpad) {
        this.seriePinpad = seriePinpad;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getModen() {
        return moden;
    }

    public void setModen(String moden) {
        this.moden = moden;
    }

    public String getNroChip() {
        return nroChip;
    }

    public void setNroChip(String nroChip) {
        this.nroChip = nroChip;
    }

    public String getNroBateria() {
        return nroBateria;
    }

    public void setNroBateria(String nroBateria) {
        this.nroBateria = nroBateria;
    }

    public String getNroTicket() {
        return nroTicket;
    }

    public void setNroTicket(String nroTicket) {
        this.nroTicket = nroTicket;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getProblemaReportado() {
        return problemaReportado;
    }

    public void setProblemaReportado(String problemaReportado) {
        this.problemaReportado = problemaReportado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getNroTerminal() {
        return nroTerminal;
    }

    public void setNroTerminal(String nroTerminal) {
        this.nroTerminal = nroTerminal;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getIdTecnico() {
        return idTecnico;
    }

    public void setIdTecnico(int idTecnico) {
        this.idTecnico = idTecnico;
    }

    public String getDescTecnico() {
        return descTecnico;
    }

    public void setDescTecnico(String descTecnico) {
        this.descTecnico = descTecnico;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public String getTituloList(){
        return "["+this.getNroTicket() + "] " + this.getComercio() + ", " + this.getDomicilio() + ", " + this.getLocalidad() + " - " + this.getTipoServicio() + " - "
                + this.getProblemaReportado();
    }
}