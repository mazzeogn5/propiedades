package com.veragi.empresa.asmobile.provider;

import android.content.Context;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.veragi.empresa.asmobile.R;
import com.veragi.empresa.asmobile.domain.PaisSpinner;
import com.veragi.empresa.asmobile.domain.TipoPropiedadSpinner;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gastonmazzeo on 5/29/18.
 */

public class TipoPropiedadesProvider {

    public static List<TipoPropiedadSpinner> tipoPropiedades = new ArrayList<TipoPropiedadSpinner>();

    public static void initialize(Context context) {
        try {

            InputStreamReader isReader = new InputStreamReader(context.getResources().openRawResource(R.raw.tipos_propiedades));
            CSVReader reader = new CSVReader(isReader, ';');
            String [] nextLine;

            tipoPropiedades.clear();
            while ((nextLine = reader.readNext()) != null) {
                tipoPropiedades.add(new TipoPropiedadSpinner(nextLine[1].toString(), Integer.parseInt(nextLine[0])));
            }


            Collections.sort(tipoPropiedades, new Comparator<TipoPropiedadSpinner>() {
                @Override
                public int compare(TipoPropiedadSpinner o1, TipoPropiedadSpinner o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "No se pudieron cargar los tipos de propiedades", Toast.LENGTH_SHORT).show();
        }
    }
}
