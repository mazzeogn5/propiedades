package com.veragi.empresa.asmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.veragi.empresa.asmobile.drawer.CustomDrawerAdapter;
import com.veragi.empresa.asmobile.drawer.DrawerItem;

import java.util.ArrayList;

/**
 * Created by gastonmazzeo on 8/7/18.
 */

public class DrawerActivity extends BaseActivity {
    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapterMenu;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private int drawerPosition = 0;

    int idUsuario;
    String mActivityTitle;
    Context contex;

    public void setContext(Context context) {
        this.contex = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initDrawer(int drawerPosition) {
        this.drawerPosition = drawerPosition;
        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();
    }

    private void addDrawerItems() {

        ArrayList<DrawerItem> dataList = new ArrayList<DrawerItem>();
        dataList.add(new DrawerItem(getString(R.string.title_activity_publicar_aviso), R.drawable.publicar_aviso_icon));
        dataList.add(new DrawerItem(getString(R.string.title_activity_mis_avisos), R.drawable.mis_publicaciones_icon));
        dataList.add(new DrawerItem(getString(R.string.title_mis_novedades), R.drawable.novedades_icon));
        dataList.add(new DrawerItem(getString(R.string.title_salir), R.drawable.salir_icon));

        mDrawerList.setAdapter(new CustomDrawerAdapter(
                this,
                R.layout.custom_drawer_item,
                dataList));

        //mDrawerList.setAdapter(mAdapterMenu);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 3) {
                    for (Activity a : LoginActivity.activities) {
                        a.finish();
                    }
                    LoginActivity.activities.clear();

                    System.exit(0);
                    android.os.Process.killProcess(android.os.Process.myPid());
                } else {
                    startActivityOrCloseDrawer(position);
                }
            }
        });
    }

    private void startActivityOrCloseDrawer(int drawerPosition) {
        if ( this.drawerPosition != drawerPosition ) {
            Class<?> activityClass = MainActivity.class;
            switch (drawerPosition) {
                case 2: activityClass = MainActivity.class;
                    break;
                case 1: activityClass = MisAvisos.class;
                    break;
                case 0: activityClass = AvisoActivity.class;
                    break;
            }

            Intent intent = new Intent(this, activityClass);
            intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));
            this.startActivity(intent);
        } else {
            this.mDrawerLayout.closeDrawers();
        }
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

}
