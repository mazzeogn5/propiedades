package com.veragi.empresa.asmobile;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.veragi.empresa.asmobile.domain.LocalidadSpinner;
import com.veragi.empresa.asmobile.domain.PaisSpinner;
import com.veragi.empresa.asmobile.domain.ProvinciaSpinner;
import com.veragi.empresa.asmobile.helper.Constantes;
import com.veragi.empresa.asmobile.provider.LocalidadesProvider;
import com.veragi.empresa.asmobile.provider.PaisesProvider;
import com.veragi.empresa.asmobile.provider.ProvinciasProvider;
import com.veragi.empresa.asmobile.provider.TipoPropiedadesProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


public class LoginActivity extends BaseActivity  implements View.OnClickListener {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private static final int READ_EXTERNAL_STORAGE_PERMISSION = 0;

    static Vector<Activity> activities = new Vector();

    @InjectView(R.id.input_usuario) EditText _emailText;
    @InjectView(R.id.input_password) EditText _passwordText;
    @InjectView(R.id.btn_login) Button _loginButton;


    int idUsuario = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        activities.add(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        _loginButton = (Button) findViewById(R.id.btn_login);
        _loginButton.setOnClickListener(this);

        this.setVersionText();

         TextView textView =(TextView)findViewById(R.id.txtRegistrarse);
        textView.setClickable(true);
        textView.setOnClickListener(new HandleRegistracion());
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                READ_EXTERNAL_STORAGE_PERMISSION);

    }

    private void setVersionText() {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            TextView versionTextView = (TextView) findViewById(R.id.version);
            versionTextView.setText("Versión "+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private class HandleRegistracion implements View.OnClickListener {
        public void onClick(View arg0) {
            Intent intent = new Intent(arg0.getContext(), RegistracionActivity.class);
            arg0.getContext().startActivity(intent);

        }
    }

    int intentosErrorLogin = 0;
    public void login(){
        this.showLoadingDialog();
            StringRequest sr = new StringRequest(Request.Method.POST, Constantes.dominio + "login/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        hideLoadingDialog();
                        JSONArray obj = new JSONArray(response);
                        for (int i = 0; i < obj.length(); i++) {
                            JSONObject jResponse = obj.getJSONObject(i);
                            idUsuario = (Integer)jResponse.getInt("id");
                            break;
                        }
                        openProfile(idUsuario);

                    }catch(Exception ex){
                        Log.d("Login", "error!");
                        ex.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if(intentosErrorLogin < Constantes.reintentos){
                        login();
                    } else {
                        Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                        hideLoadingDialog();
                    }

                    intentosErrorLogin++;

                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", _emailText.getText().toString());
                    params.put("password",  _passwordText.getText().toString());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            sr.setRetryPolicy(new DefaultRetryPolicy(10000,
                10,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(sr);

    }

    private void openProfile(int idUsuario){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));

        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if(_emailText.getText().toString().trim().length() < 1) {
            _emailText.setError("Ingrese su usuario");
        }else if(_passwordText.getText().toString().trim().length() < 1) {
            _passwordText.setError("Ingrese la contraseña");
        }else {
            login();
        }

    }



}