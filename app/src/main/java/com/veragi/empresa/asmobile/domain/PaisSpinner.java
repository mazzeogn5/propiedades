package com.veragi.empresa.asmobile.domain;

import com.veragi.empresa.asmobile.helper.Spinner;

import java.io.Serializable;

/**
 * Created by egimenez on 21/07/2016.
 */
public class PaisSpinner extends Spinner implements Serializable {
    public String string;
    public Object tag;

    public PaisSpinner(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}
