package com.veragi.empresa.asmobile.domain;

import com.veragi.empresa.asmobile.helper.Spinner;

import java.io.Serializable;

/**
 * Created by egimenez on 24/09/2016.
 */
public class LocalidadSpinner extends Spinner implements Serializable {
    public String string;
    public Object tag;
    public int paisId;
    public int provinciaId;

    public LocalidadSpinner(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    public LocalidadSpinner(String string, Object tag, int paisId, int provinciaId) {
        this.string = string;
        this.tag = tag;
        this.paisId = paisId;
        this.provinciaId = provinciaId;
    }

    @Override
    public String toString() {
        return string;
    }
}
