package com.veragi.empresa.asmobile;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.veragi.empresa.asmobile.domain.LocalidadSpinner;
import com.veragi.empresa.asmobile.domain.Novedad;
import com.veragi.empresa.asmobile.domain.PaisSpinner;
import com.veragi.empresa.asmobile.domain.Propiedad;
import com.veragi.empresa.asmobile.domain.ProvinciaSpinner;
import com.veragi.empresa.asmobile.domain.TipoPropiedadSpinner;
import com.veragi.empresa.asmobile.helper.Constantes;
import com.veragi.empresa.asmobile.helper.CustomJSONObjectArrayRequest;
import com.veragi.empresa.asmobile.helper.CustomVolleyRequestQueue;
import com.veragi.empresa.asmobile.provider.PaisesProvider;
import com.veragi.empresa.asmobile.provider.TipoPropiedadesProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class MisAvisos extends DrawerActivity implements Response.Listener,
        Response.ErrorListener{

    private RecyclerView recyclerView;
    private CardView cardView;
    private RequestQueue mQueue;
    private Toolbar toolbar;
    private RecyclerView.Adapter mAdapter;
    public static final String REQUEST_TAG = "MisAvisos";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_avisos);
        LoginActivity.activities.add(this);
        cargarPaises();
        cargarTipoPropiedad();
        idUsuario = (String)(getIntent().getStringExtra("ID_USUARIO"))!=null? Integer.parseInt((String)(getIntent().getStringExtra("ID_USUARIO"))) : 0;
        this.initDrawer(1);
        mActivityTitle = getTitle().toString();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initContrls();

        cargarAvisos();

    }


    private void initContrls() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  cardView = (CardView) findViewById(R.id.cardList);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        if (toolbar != null) {
            //  setSupportActionBar(toolbar);
            //getSupportActionBar().setTitle("Android Versions");

        }


        recyclerView.setHasFixedSize(true);

        // ListView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    int intentosErrorAvisos = 0;
    private void cargarAvisos(){
        this.hideError();
        this.showLoadingDialog("Cargando mis avisos.");
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();

        //http://www.xn--zonadueo-j3a.com/restci-zdn/index.php/api/propiedades/user_id/[ID_USER]

        //String url = "http://www.xn--zonadueo-j3a.com/restci-zdn/index.php/api/propiedades/user_id/" +idUsuario;
        String url = Constantes.dominio + "propiedades/id/0/user_id/"+idUsuario;

        Log.d("URL Mis publicaciones", url);
        final CustomJSONObjectArrayRequest jsonRequest = new CustomJSONObjectArrayRequest(Request.Method
                .GET, url,
                new JSONObject(), this, this);
        jsonRequest.setTag(REQUEST_TAG);
        mQueue.add(jsonRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.hideLoadingDialog();
        if (mQueue != null) {
            mQueue.cancelAll(REQUEST_TAG);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("ERROR", "onErrorResponse"+error.getLocalizedMessage());
        if(intentosErrorAvisos< Constantes.reintentos){
            cargarAvisos();
        }else{
            this.hideLoadingDialog();
            this.showError();
        }

        intentosErrorAvisos++;

    }

    @Override
    public void onResponse(Object response) {
        Log.d("onResponse", "onResponse");
        ArrayList<Propiedad> propiedades = new ArrayList<Propiedad>();
        try {
            JSONArray ja = (JSONArray)response;

            for(int i = 0; i < ((JSONArray)response).length(); i++){
                JSONObject jResponse = ja.getJSONObject(i);

                Propiedad prop = new Propiedad();
                prop.setId(jResponse.getInt("id"));
                prop.setRef(jResponse.getString("ref"));
                prop.setPro_name(jResponse.getString("pro_name"));
                prop.setPro_alias(jResponse.getString("pro_alias"));
                prop.setCategory_id(jResponse.getString("category_id"));
                prop.setPrice(jResponse.getString("price"));
                prop.setPro_small_desc(jResponse.getString("pro_small_desc"));
                prop.setPro_full_desc(jResponse.getString("pro_full_desc"));
                prop.setPro_type(jResponse.getString("pro_type"));
                prop.setAddress(jResponse.getString("address"));
                prop.setCity(jResponse.getString("city"));
                prop.setState(jResponse.getString("state"));
                prop.setPro_alias(jResponse.getString("pro_alias"));
                prop.setCountry(jResponse.getString("country"));
                prop.setPostcode(jResponse.getString("postcode"));
                prop.setIdUsuario(Integer.toString(idUsuario));
                prop.setPaises(paises);
                prop.setTipoPropiedades(tipoPropiedades);

                boolean falloImagenPrincipal = false;
                String path = "http://www.xn--zonadueo-j3a.com/images/osproperty/properties/"+prop.getId()+"/medium/";
                try {
                    JSONObject jImagen = jResponse.getJSONObject("imagen_principal");
                    prop.setImagen(jImagen.getString("image") != null && !"".equals(jImagen.getString("image")) ? path+jImagen.getString("image") : "sin_imagen");

                }catch(Exception ex){
                    falloImagenPrincipal = true;
                }

                try {
                    JSONArray galeria = jResponse.getJSONArray("galeria");
                    for (int j = 0; j < galeria.length(); j++) {
                        JSONObject imagen = galeria.getJSONObject(j);
                        if (j == 0 && falloImagenPrincipal) {
                            prop.setImagen(imagen.getString("image") != null && !"".equals(imagen.getString("image")) ? path+imagen.getString("image") : "sin_imagen");
                        }

                        if (j == 1) {
                            prop.setImagen2(imagen.getString("image") != null && !"".equals(imagen.getString("image")) ? path+imagen.getString("image") : "sin_imagen");
                        }

                        if (j == 2) {
                            prop.setImagen3(imagen.getString("image") != null && !"".equals(imagen.getString("image")) ? path+imagen.getString("image") : "sin_imagen");
                        }
                    }
                }catch(Exception ex){
                    prop.setImagen("sin_imagen");
                    prop.setImagen2("sin_imagen");
                    prop.setImagen3("sin_imagen");

                }

                propiedades.add(prop);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.hideLoadingDialog();
        mAdapter = new CardViewDataAvisoAdapter(propiedades);
        recyclerView.setAdapter(mAdapter);

    }



    //Carga de Listas
    ArrayList<PaisSpinner> paises = new ArrayList<PaisSpinner>();
    int intentosErrorPaises = 0;
    private void cargarPaises(){
        if(paises==null || paises.isEmpty()) {
            paises = new ArrayList<PaisSpinner>();
            int index = 0;
            int posEdit = 0;
            for (PaisSpinner pais : PaisesProvider.paises) {
                paises.add(pais);
                index++;
            }
        }
    }

    int intentosErrorTiposPropiedad = 0;
    ArrayList<TipoPropiedadSpinner> tipoPropiedades = new ArrayList<TipoPropiedadSpinner>();
    private void cargarTipoPropiedad(){
        if(tipoPropiedades==null || tipoPropiedades.isEmpty()) {
            tipoPropiedades = new ArrayList<TipoPropiedadSpinner>();
            int index = 0;
            int posEdit = 0;
            for (TipoPropiedadSpinner tipoPropiedad : TipoPropiedadesProvider.tipoPropiedades) {
                tipoPropiedades.add(tipoPropiedad);
                index++;
            }
        }
    }

}
