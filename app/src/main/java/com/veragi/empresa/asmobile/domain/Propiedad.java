package com.veragi.empresa.asmobile.domain;

import java.util.ArrayList;

/**
 * Created by egimenez on 22/09/2016.
 */
public class Propiedad {
    private int id;
    private String ref ;
    private String pro_name ;
    private String pro_alias  ;// podes recortar a 10 sino lo hago yo.
    private String agent_id   ;// aqui ira el ID del usuario AGENTE ( id tabla agentes q es la q va relacionada con user_id) ;// TE VOY A TENER Q HACER UN REST para q consultes la tabla agentes x user_id y te devuelvo el id y sus campos asi los podes usar en este alta.
    private String category_id   ;// aca deberia ir id Tipo Propiedad (Depto/Casa/Terreno, esta en el REST)
    private String price ;// precio
    private String pro_small_desc  ;// descripcion pequeña
    private String pro_full_desc  ;// copiar la pequeña en la FULL (FULL NO MOSTRAR) asi dsps amplia
    private String pro_type  ;// dsps te digo (no recuerdo ahora)
    private String address;
    private String city;
    private String state;
    private String country;
    private String postcode;
    private String idUsuario;
    private String imagen;
    private String imagen2;
    private String imagen3;
    private String imagen4;
    ArrayList<PaisSpinner> paises;
    ArrayList<TipoPropiedadSpinner> tipoPropiedades;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_alias() {
        return pro_alias;
    }

    public void setPro_alias(String pro_alias) {
        this.pro_alias = pro_alias;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPro_small_desc() {
        return pro_small_desc;
    }

    public void setPro_small_desc(String pro_small_desc) {
        this.pro_small_desc = pro_small_desc;
    }

    public String getPro_full_desc() {
        return pro_full_desc;
    }

    public void setPro_full_desc(String pro_full_desc) {
        this.pro_full_desc = pro_full_desc;
    }

    public String getPro_type() {
        return pro_type;
    }

    public void setPro_type(String pro_type) {
        this.pro_type = pro_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getImagen2() {
        return imagen2;
    }

    public void setImagen2(String imagen2) {
        this.imagen2 = imagen2;
    }

    public String getImagen3() {
        return imagen3;
    }

    public void setImagen3(String imagen3) {
        this.imagen3 = imagen3;
    }

    public String getImagen4() {
        return imagen4;
    }

    public void setImagen4(String imagen4) {
        this.imagen4 = imagen4;
    }

    public ArrayList<PaisSpinner> getPaises() {
        return paises;
    }

    public void setPaises(ArrayList<PaisSpinner> paises) {
        this.paises = paises;
    }

    public ArrayList<TipoPropiedadSpinner> getTipoPropiedades() {
        return tipoPropiedades;
    }

    public void setTipoPropiedades(ArrayList<TipoPropiedadSpinner> tipoPropiedades) {
        this.tipoPropiedades = tipoPropiedades;
    }
}
