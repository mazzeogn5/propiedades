package com.veragi.empresa.asmobile.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

/**
 * Created by egimenez on 03/10/2016.
 */
public class MyNetworkTask extends AsyncTask<URL, Void, Bitmap> {

    ImageView tIV;

    public MyNetworkTask(ImageView iv){
        tIV = iv;
    }

    @Override
    protected Bitmap doInBackground(URL... urls) {

        Bitmap networkBitmap = null;

        URL networkUrl = urls[0]; //Load the first element
        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            // networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream());
            networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream(), null, options);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return networkBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        tIV.setImageBitmap(result);
    }

}