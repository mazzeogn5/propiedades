package com.veragi.empresa.asmobile.provider;

import android.content.Context;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.veragi.empresa.asmobile.R;
import com.veragi.empresa.asmobile.domain.ProvinciaSpinner;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by gastonmazzeo on 5/29/18.
 */

public class ProvinciasProvider {

    public static List<ProvinciaSpinner> provincias = new ArrayList<ProvinciaSpinner>();

    public static void initialize(Context context) {
        try {

            InputStreamReader isReader = new InputStreamReader(context.getResources().openRawResource(R.raw.provincias));
            CSVReader reader = new CSVReader(isReader, ';');
            String [] nextLine;

            provincias.clear();
            while ((nextLine = reader.readNext()) != null) {
                //PROVINCIA, PROVINCIA_ID, PAIS_ID
                provincias.add(new ProvinciaSpinner(nextLine[2].toString(), Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1])));
            }

            Collections.sort(provincias, new Comparator<ProvinciaSpinner>() {
                @Override
                public int compare(ProvinciaSpinner o1, ProvinciaSpinner o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(context, "No se pudieron cargar las provincias", Toast.LENGTH_SHORT).show();
        }
    }
}
