package com.veragi.empresa.asmobile;

import com.veragi.empresa.asmobile.domain.FeedProperties;

import java.util.ArrayList;

/**
 * Created by egimenez on 06/04/2016.
 */
public class ASMobileService {

    private static ASMobileService instance = null;

    public static ASMobileService getInstance() {
        if(instance == null) {
            instance = new ASMobileService();
        }
        return instance;
    }

    public ArrayList<FeedProperties> getPedidos(String nombreUsuario){
        ArrayList<FeedProperties> pedidos = new ArrayList<>();
        for(int i=0; i<15; i++){
            FeedProperties feed = new FeedProperties();
            feed.setNroTicket("WOO-12345" + i);
            feed.setComercio("Nombre comercio: " + nombreUsuario);
            feed.setCp("1455");
            feed.setDomicilio("Calle " + i);
            feed.setLocalidad("Localidad " + i);
            feed.setProvincia("Provincia " + i);
            feed.setTelefono("4567-123" + i);
            feed.setHorarioVisita("lunes a viernes de 9 a 19hs");
            feed.setTipoServicio(isParImpar(i) ? "INSUMOS" : "SERVICIO TECNICO");
            feed.setProblemaReportado(isParImpar(i) ? "PEDIDO DE INSUMOS" : "REPROGRAMACION");
            feed.setObservaciones("Observacion 1");
            feed.setThumbnail(isParImpar(i) ? R.drawable.visa_icon : R.drawable.amex);

            feed.setSerieTerminal("Serie terminal " + i);
            feed.setSerieImpresora("Serie impresora " + i);
            feed.setSeriePinpad("Serie pinpad " + i);
            feed.setHost("Serie host " + i);
            feed.setModen("Moden " + i);
            feed.setNroChip("Nro chip " + i);
            feed.setNroBateria("Nro bateria " + i);

            pedidos.add(feed);

        }

        return pedidos;
    }

    private boolean isParImpar(int a){
        if(a%2==0){
           return true;
        }else{
            return false;
        }
    }

    public boolean isValidUser(String nombreUsuario, String pass){
        return true;
    }






}
