package com.veragi.empresa.asmobile;

/**
 * Created by egimenez on 01/04/2016.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.veragi.empresa.asmobile.domain.Novedad;
import com.veragi.empresa.asmobile.domain.Propiedad;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


class CardViewDataAvisoAdapter extends RecyclerView.Adapter<CardViewDataAvisoAdapter.ViewHolderAviso> {

    private static ArrayList<Propiedad> dataSet;

    public CardViewDataAvisoAdapter(ArrayList<Propiedad> os_versions) {

        dataSet = os_versions;
    }


    @Override
    public CardViewDataAvisoAdapter.ViewHolderAviso onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.card_view_aviso, null);

        ViewHolderAviso viewHolder = new ViewHolderAviso(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewDataAvisoAdapter.ViewHolderAviso viewHolder, int i) {

        Propiedad fp = dataSet.get(i);

        viewHolder.titulo.setText(fp.getPro_name());
        viewHolder.price.setText(fp.getPrice());
        viewHolder.smallDesc.setText(fp.getPro_small_desc());
        viewHolder.address.setText(fp.getAddress());


        //Load bitmap from internet
        if(!"sin_imagen".equals(fp.getImagen())) {
            try {
                URL onLineURL = new URL(fp.getImagen());
                new MyNetworkTask(viewHolder.iconView).execute(onLineURL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }else{
            viewHolder.iconView.setImageResource(R.drawable.sinimg);
        }

        viewHolder.feed = fp;
    }

    private class MyNetworkTask extends AsyncTask<URL, Void, Bitmap> {

        ImageView tIV;

        public MyNetworkTask(ImageView iv){
            tIV = iv;
        }

        @Override
        protected Bitmap doInBackground(URL... urls) {

            Bitmap networkBitmap = null;

            URL networkUrl = urls[0]; //Load the first element
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inSampleSize = 2;
                // networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream());
                networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream(), null, options);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return networkBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(tIV!=null && result!=null)
                tIV.setImageBitmap(result);
        }

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolderAviso extends RecyclerView.ViewHolder {

        public TextView titulo;
        public TextView price;
        public TextView smallDesc;
        public TextView address;
        public ImageView iconView;

        public Propiedad  feed;

        public ViewHolderAviso(View itemLayoutView) {
            super(itemLayoutView);

            titulo = (TextView) itemLayoutView.findViewById(R.id.titulo);
            price = (TextView) itemLayoutView.findViewById(R.id.price);
            smallDesc = (TextView) itemLayoutView.findViewById(R.id.smallDesc);
            address = (TextView) itemLayoutView.findViewById(R.id.address);
            iconView = (ImageView) itemLayoutView.findViewById(R.id.iconIdAviso);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), AvisoActivity.class);
                    intent.putExtra("titulo", feed.getPro_name());
                    intent.putExtra("address", feed.getAddress());
                    intent.putExtra("postcode", feed.getPostcode());
                    intent.putExtra("city", feed.getCity());
                    intent.putExtra("state", feed.getState());
                    intent.putExtra("country", feed.getCountry());
                    intent.putExtra("precio", feed.getPrice());
                    intent.putExtra("pro_small_desc", feed.getPro_small_desc());
                    intent.putExtra("pro_full_desc", feed.getPro_full_desc());
                    intent.putExtra("pro_type", feed.getPro_type());
                    intent.putExtra("idPublicacion", feed.getId());
                    intent.putExtra("imagen", feed.getImagen());
                    intent.putExtra("imagen2", feed.getImagen2());
                    intent.putExtra("imagen3", feed.getImagen3());
                    intent.putExtra("ID_USUARIO", feed.getIdUsuario());
                    intent.putExtra("paises", feed.getPaises());
                    intent.putExtra("tipoPropiedades", feed.getTipoPropiedades());

                    v.getContext().startActivity(intent);

                }
            });


        }

    }





}
