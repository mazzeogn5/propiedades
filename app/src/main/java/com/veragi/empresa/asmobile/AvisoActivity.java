package com.veragi.empresa.asmobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.internal.bind.ArrayTypeAdapter;
import com.veragi.empresa.asmobile.domain.LocalidadSpinner;
import com.veragi.empresa.asmobile.domain.PaisSpinner;
import com.veragi.empresa.asmobile.domain.ProvinciaSpinner;
import com.veragi.empresa.asmobile.domain.TipoPropiedadSpinner;
import com.veragi.empresa.asmobile.ftp.FileTransfer;
import com.veragi.empresa.asmobile.ftp.HDWFTP_Upload;
import com.veragi.empresa.asmobile.helper.Constantes;
import com.veragi.empresa.asmobile.helper.CustomJSONObjectArrayRequest;
import com.veragi.empresa.asmobile.helper.CustomVolleyRequestQueue;
import com.veragi.empresa.asmobile.helper.UploadToFtp;
import com.veragi.empresa.asmobile.provider.LocalidadesProvider;
import com.veragi.empresa.asmobile.provider.PaisesProvider;
import com.veragi.empresa.asmobile.provider.ProvinciasProvider;
import com.veragi.empresa.asmobile.provider.TipoPropiedadesProvider;

import org.apache.commons.net.ftp.FTPClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AvisoActivity extends BaseActivity {
    Boolean isAlta = false;
    int idPublicacion;
    int idUsuario;
    int idPaisEdit = 0;
    int idProvinciaEdit = 0;
    int idLocalidadEdit = 0;
    int tipoPropiedadEdit = 0;
    int idPaisSelected = 0;

    ViewFlipper viewFlipper;
    Button next;
    Button previous;
    private ImageLoadingUtils utils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aviso);
        LoginActivity.activities.add(this);

        PaisesProvider.initialize(this.getApplicationContext());
        ProvinciasProvider.initialize(this.getApplicationContext());
        LocalidadesProvider.initialize(this.getApplicationContext());
        TipoPropiedadesProvider.initialize(this.getApplicationContext());

        findViewById(R.id.viewFlipper).setEnabled(false);
        findViewById(R.id.boton_modificar_guardar).setEnabled(false);
        utils = new ImageLoadingUtils(this);

        idUsuario = ((String)getIntent().getStringExtra("ID_USUARIO"))!=null? Integer.parseInt((String)getIntent().getStringExtra("ID_USUARIO")):0;
        idPublicacion = getIntent().getIntExtra("idPublicacion", 0);

        if(idPublicacion==0){
            isAlta = true;
            ((Button)findViewById(R.id.boton_modificar_guardar)).setText("Publicar");
            findViewById(R.id.boton_eliminar).setVisibility(View.INVISIBLE);
            ((Button)findViewById(R.id.boton_eliminar)).setWidth(0);
            setTitle("Crear aviso");
        }else{
            ((Button)findViewById(R.id.boton_modificar_guardar)).setText("Modificar");
            setTitle("Mi aviso");
        }

        cargarFotos();

        ((TextView) findViewById(R.id.campo_domicilio)).setText((String) getIntent().getStringExtra("address"));
        ((TextView) findViewById(R.id.campo_codigoPostal)).setText((String) getIntent().getStringExtra("postcode"));
        ((TextView) findViewById(R.id.campo_titulo)).setText((String) getIntent().getStringExtra("titulo"));//pro_name
        ((TextView) findViewById(R.id.campo_small_desc)).setText((String) getIntent().getStringExtra("pro_small_desc"));
        ((TextView) findViewById(R.id.campo_full_desc)).setText((String) getIntent().getStringExtra("pro_full_desc"));
        ((TextView) findViewById(R.id.campo_precio)).setText((String) getIntent().getStringExtra("precio"));

        idPaisEdit = (String)getIntent().getStringExtra("country")!=null?Integer.parseInt((String)getIntent().getStringExtra("country")):0;
        idProvinciaEdit = (String)getIntent().getStringExtra("state")!=null?Integer.parseInt((String)getIntent().getStringExtra("state")):0;
        idLocalidadEdit = (String)getIntent().getStringExtra("city")!=null?Integer.parseInt((String)getIntent().getStringExtra("city")):0;
        tipoPropiedadEdit = (String)getIntent().getStringExtra("pro_type")!=null?Integer.parseInt((String)getIntent().getStringExtra("pro_type")):0;

        paises = (ArrayList<PaisSpinner>) getIntent().getSerializableExtra("paises");
        provincias = (ArrayList<ProvinciaSpinner>) getIntent().getSerializableExtra("provincias");
        localidades = (ArrayList<LocalidadSpinner>) getIntent().getSerializableExtra("localidades");
        tipoPropiedades = (ArrayList<TipoPropiedadSpinner>) getIntent().getSerializableExtra("tipoPropiedades");

        spinnerPaises = (Spinner) findViewById(R.id.campo_pais);
        spinnerProvincias = (Spinner) findViewById(R.id.campo_provincia);
        spinnerLocalidades = (Spinner) findViewById(R.id.campo_localidad);
        spinnerTipoPropiedad = (Spinner) findViewById(R.id.campo_tipoPropiedad);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // FIND PAISES
        cargarPaises();
        // FIND TIPO PROPIEDAD
        cargarTipoPropiedad();

         spinnerPaises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                 PaisSpinner pais = paises.get(spinnerPaises.getSelectedItemPosition());
                 idPaisSelected = (Integer) pais.tag;
                 cargarProvincias(idPaisSelected, true);
             }

             @Override
             public void onNothingSelected(AdapterView<?> parentView) {}
         });
        spinnerPaises.setSelection(8);


        spinnerProvincias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (provincias == null) {
                    provincias = new ArrayList<ProvinciaSpinner>();
                }

                if (provincias.size() > 0) {
                    ProvinciaSpinner provincia = provincias.get(spinnerProvincias.getSelectedItemPosition());
                    if ((Integer) provincia.tag > -1) {
                        if ("fotosActivity".equals((String) getIntent().getStringExtra("fromActivity"))) {
                            getIntent().putExtra("fromActivity", "avisoActivity");
                            cargarLocalidades((Integer) provincia.tag, false);
                        } else {
                            cargarLocalidades((Integer) provincia.tag, true);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });


        //Edicion
        findViewById(R.id.boton_modificar_guardar).setOnClickListener(new HandleClickModificarGuardar());
        findViewById(R.id.boton_eliminar).setOnClickListener(new HandleClickEliminar());

        //Volver
        findViewById(R.id.boton_volver).setOnClickListener(new HandleClickVolver());

        //Foto
        viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        next = (Button) findViewById(R.id.next);
        previous = (Button) findViewById(R.id.previous);
        next.setOnClickListener(new HandleClickSiguienteImg());
        previous.setOnClickListener(new HandleClickAnteriorImg());
        findViewById(R.id.viewFlipper).setOnClickListener(new HandleClickEditarFotos());

    }




    private class HandleClickEditarFotos implements View.OnClickListener {
        public void onClick(View arg0) {
            Intent intent = new Intent(arg0.getContext(), FotosActivity.class);
            intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));
            intent.putExtra("pro_name", ((EditText) findViewById(R.id.campo_titulo)).getText().toString());
            //params.put("pro_alias", ((EditText) findViewById(R.id.campo_correo)).getText().toString());
            intent.putExtra("price", ((EditText) findViewById(R.id.campo_precio)).getText().toString());
            //params.put("curr", ((EditText) findViewById(R.id.campo_rpass)).getText().toString());
            intent.putExtra("pro_small_desc", ((EditText) findViewById(R.id.campo_small_desc)).getText().toString());
            intent.putExtra("pro_full_desc", ((EditText) findViewById(R.id.campo_full_desc)).getText().toString());

            intent.putExtra("address", ((EditText) findViewById(R.id.campo_domicilio)).getText().toString());

            PaisSpinner pais = paises.get(spinnerPaises.getSelectedItemPosition());
            ProvinciaSpinner provincia = provincias.get(spinnerProvincias.getSelectedItemPosition());
            LocalidadSpinner localidad = localidades.get(spinnerLocalidades.getSelectedItemPosition());
            intent.putExtra("city", ((Integer)localidad.tag) <0? "-1" : ((Integer)localidad.tag));
            intent.putExtra("state", ((Integer)provincia.tag) <0? "-1" : ((Integer)provincia.tag));
            intent.putExtra("country", ((Integer)pais.tag) <0? "-1" : ((Integer)pais.tag));

            TipoPropiedadSpinner tipoPropiedad = tipoPropiedades.get(spinnerTipoPropiedad.getSelectedItemPosition());
            intent.putExtra("pro_type", ((Integer)tipoPropiedad.tag) <0? "-1" : ((Integer)tipoPropiedad.tag));

            intent.putExtra("postcode", ((EditText) findViewById(R.id.campo_codigoPostal)).getText().toString());
            intent.putExtra("idPublicacion", idPublicacion);

            intent.putExtra("imagen", urlImg1 != null ? urlImg1 : null);
            intent.putExtra("imagen2", urlImg2 != null ? urlImg2 : null);
            intent.putExtra("imagen3", urlImg3 != null ? urlImg3 : null);

            intent.putExtra("paises", paises);
            intent.putExtra("provincias", provincias);
            intent.putExtra("localidades", localidades);
            intent.putExtra("tipoPropiedades", tipoPropiedades);

            arg0.getContext().startActivity(intent);


        }
    }


    private class HandleClickSiguienteImg implements View.OnClickListener {
        public void onClick(View arg0) {
            viewFlipper.showPrevious();
         }
    }

    private class HandleClickAnteriorImg implements View.OnClickListener {
        public void onClick(View arg0) {
            viewFlipper.showNext();
        }
    }

    private class HandleClickVolver implements View.OnClickListener {
        public void onClick(View arg0) {
            Intent intent = new Intent(arg0.getContext(), MisAvisos.class);
            intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));
            arg0.getContext().startActivity(intent);
        }
    }

    private String agregarSeparadorImg(String imgConcatenadas, String img){
        if (img!=null && !"sin_imagen".equals(img)) {
            if (imgConcatenadas!=null && !"".equals(imgConcatenadas)) {
                imgConcatenadas = "@@"+img;
            } else {
                imgConcatenadas = img;
            }
         }
        return imgConcatenadas;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void guardarImagenes(final int idPublicacion, final View arg0){
        String imagenes = "";
        try{
            String imagen = getIntent().getStringExtra("imagen");
            if(imagen!=null && !"sin_imagen".equals(imagen)){
                if(!imagen.contains("http")) {
                    imagen = new ImageCompressionAsyncTask(true).execute(getRealPathFromURI(Uri.parse(imagen))).get();
                    new HDWFTP_Upload(this.getApplicationContext()).execute(imagen, imagen.substring(imagen.lastIndexOf("/") + 1), "/properties/" + idPublicacion + "/medium");
                }
                imagenes=imagen.substring(imagen.lastIndexOf("/") + 1);
            }

            String imagen2 = getIntent().getStringExtra("imagen2");
            if(imagen2!=null && !"sin_imagen".equals(imagen2)){
                if(!imagen2.contains("http")) {
                    imagen2 = new ImageCompressionAsyncTask(true).execute(getRealPathFromURI(Uri.parse(imagen2))).get();
                    new HDWFTP_Upload(this.getApplicationContext()).execute(imagen2, imagen2.substring(imagen2.lastIndexOf("/") + 1), "/properties/" + idPublicacion + "/medium");
                }
                imagenes+=agregarSeparadorImg(imagenes, imagen2.substring(imagen2.lastIndexOf("/") + 1));
            }

            String imagen3 = getIntent().getStringExtra("imagen3");
            if(imagen3!=null && !"sin_imagen".equals(imagen3)){
                if(!imagen3.contains("http")) {
                    imagen3 = new ImageCompressionAsyncTask(true).execute(getRealPathFromURI(Uri.parse(imagen3))).get();
                    new HDWFTP_Upload(this.getApplicationContext()).execute(imagen3, imagen3.substring(imagen3.lastIndexOf("/") + 1), "/properties/" + idPublicacion + "/medium");
                }
                imagenes+=agregarSeparadorImg(imagenes, imagen3.substring(imagen3.lastIndexOf("/") + 1));

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        guardarImagenesRest(idPublicacion, imagenes, arg0);



    }

    int intentosGuardarImagen = 0;
    private void guardarImagenesRest(final int idPublicacion, final String imagenes, final View arg0){
        StringRequest sr = new StringRequest(Request.Method.POST, Constantes.dominio + "propiedadesimagenes/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    String message = ((JSONObject) obj).getString("message");
                    //Toast.makeText(AvisoActivity.this, message, Toast.LENGTH_LONG).show();

                }catch(Exception ex){
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(intentosGuardarImagen < Constantes.reintentos){
                    guardarImagenesRest(idPublicacion, imagenes, arg0);
                }

                intentosGuardarImagen++;



            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("pro_id", Integer.toString(idPublicacion));
                params.put("image", imagenes);
                params.put("image_desc", "galeria de imagenes");
                params.put("ordering", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(arg0.getContext());
        queue.add(sr);

    }


    private void confirmModificarGuardarDialog(final View arg0) {

        final PaisSpinner _pais = paises.get(spinnerPaises.getSelectedItemPosition());
        final ProvinciaSpinner _provincia = provincias.get(spinnerProvincias.getSelectedItemPosition());
        final LocalidadSpinner _localidad = localidades.get(spinnerLocalidades.getSelectedItemPosition());
        final TipoPropiedadSpinner _tipoPropiedad = tipoPropiedades.get(spinnerTipoPropiedad.getSelectedItemPosition());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setMessage(isAlta?"Desea publicar el aviso?" : "Desea modificar el aviso?")
                .setPositiveButton("Si",  new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        showLoadingDialog("Guardando aviso. Por favor espere.");
                        // Toast.makeText(AvisoActivity.this, "Guardando aviso...", Toast.LENGTH_LONG).show();
                        //mPostCommentResponse.requestStarted();
                        StringRequest sr = new StringRequest(isAlta ? Request.Method.POST : Request.Method.PUT, Constantes.dominio + "propiedades/id/"+idPublicacion, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    JSONObject obj = new JSONObject(response);
                                    String message = ((JSONObject) obj).getString("message");
                                    String datosUtilizados = obj.getString("datos_utilizados");

                                    JSONObject datosUtilizadosJson = new JSONObject(datosUtilizados);
                                    idPublicacion = ((JSONObject) datosUtilizadosJson).getInt("id");
                                    guardarImagenes(idPublicacion, arg0);
                                    Toast.makeText(AvisoActivity.this, message, Toast.LENGTH_LONG).show();


                                }catch(Exception ex){
                                    ex.printStackTrace();
                                }
                                hideLoadingDialog();
                                Intent intent = new Intent(arg0.getContext(), MisAvisos.class);
                                intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));
                                startActivity(intent);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                hideLoadingDialog();
                                if(isAlta)
                                    Toast.makeText(AvisoActivity.this, "Ocurrio un error al crear el aviso  ", Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(AvisoActivity.this, "Ocurrio un error al modificar el aviso  ", Toast.LENGTH_LONG).show();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {



                                Map<String, String> params = new HashMap<String, String>();
                                params.put("pro_name", ((EditText) findViewById(R.id.campo_titulo)).getText().toString());
                                params.put("pro_alias", "");
                                params.put("price", ((EditText) findViewById(R.id.campo_precio)).getText().toString());
                                params.put("curr", "");
                                params.put("pro_small_desc", ((EditText) findViewById(R.id.campo_small_desc)).getText().toString());
                                params.put("pro_full_desc", ((EditText) findViewById(R.id.campo_full_desc)).getText().toString());
                                params.put("pro_type", _tipoPropiedad.tag+"");
                                params.put("address", ((EditText) findViewById(R.id.campo_domicilio)).getText().toString());
                                params.put("city", _localidad.tag + "");
                                params.put("state", _provincia.tag+"");
                                params.put("country", _pais.tag + "");
                                params.put("postcode", ((EditText) findViewById(R.id.campo_codigoPostal)).getText().toString());
                                params.put("show_address", "");
                                params.put("bed_room", "");
                                params.put("bath_room", "");
                                params.put("rooms", "");
                                params.put("square_feet", "");
                                params.put("number_of_floors", "");
                                if(isAlta)
                                    params.put("user_id", Integer.toString(idUsuario));
                                else
                                    params.put("id", Integer.toString(idPublicacion));

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Content-Type", "application/x-www-form-urlencoded");
                                return params;
                            }
                        };
                        RequestQueue queue = Volley.newRequestQueue(arg0.getContext());
                        queue.add(sr);

            /*}else{
                AlertDialog.Builder b=new AlertDialog.Builder(AvisoActivity.this);
                b.setMessage("Internet connectivity failure.Try again!");
                b.show();
            }*/

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private class HandleClickModificarGuardar implements View.OnClickListener {
        public void onClick(final View arg0) {
            //Validacion de campos
            if(((EditText) findViewById(R.id.campo_titulo)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_titulo)).setError("El campo es obligatorio");
            }else if(((EditText) findViewById(R.id.campo_domicilio)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_domicilio)).setError("El campo es obligatorio");
            }else if(((EditText) findViewById(R.id.campo_codigoPostal)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_codigoPostal)).setError("El campo es obligatorio");
            }else if(((EditText) findViewById(R.id.campo_codigoPostal)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_codigoPostal)).setError("El campo es obligatorio");
            }else if(((EditText) findViewById(R.id.campo_small_desc)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_small_desc)).setError("El campo es obligatorio");
            }else if(((EditText) findViewById(R.id.campo_full_desc)).getText().toString().trim().length() < 1) {
                ((EditText) findViewById(R.id.campo_full_desc)).setError("El campo es obligatorio");
            }else {

                confirmModificarGuardarDialog(arg0);
            }

        }

    }


    private void confirmRemoveDialog(final View arg0) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setMessage("Desea eliminar la publicación?")
                .setPositiveButton("Si",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        StringRequest sr = new StringRequest(Request.Method.DELETE, Constantes.dominio + "propiedades/id/"+idPublicacion, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject obj = new JSONObject(response);
                                    String message = ((JSONObject) obj).getString("message");
                                    Toast.makeText(AvisoActivity.this, message, Toast.LENGTH_LONG).show();

                                }catch(Exception ex){
                                    ex.printStackTrace();
                                }

                                Intent intent = new Intent(arg0.getContext(), MisAvisos.class);
                                intent.putExtra("ID_USUARIO", Integer.toString(idUsuario));
                                arg0.getContext().startActivity(intent);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(AvisoActivity.this, "Ocurrio un error al eliminar el aviso", Toast.LENGTH_LONG).show();
                            }
                        }) ;
                        RequestQueue queue = Volley.newRequestQueue(arg0.getContext());
                        queue.add(sr);



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private class HandleClickEliminar implements View.OnClickListener {
        public void onClick(final View arg0) {
            confirmRemoveDialog(arg0);

        }
    }

    Spinner spinnerPaises;
    ArrayList<PaisSpinner> paises = new ArrayList<PaisSpinner>();
    int intentosErrorPaises = 0;
    private void cargarPaises(){
        if(paises==null || paises.isEmpty()) {
            paises = new ArrayList<PaisSpinner>();
            int index = 0;
            int posEdit = 0;
            for (PaisSpinner pais : PaisesProvider.paises) {
                paises.add(pais);
                if (idPaisEdit == (Integer) pais.tag) {
                    posEdit = index;
                }
                index++;
            }
            loadPaises(posEdit);
        }else{
            int posEdit = 0;
            int i = 0;
            for (PaisSpinner ps : paises) {
                if (idPaisEdit == (int)ps.tag) {
                    posEdit = i;
                }
                i++;
            }
            loadPaises(posEdit);
        }
    }

    ImageView img1;
    ImageView img2;
    ImageView img3;
    String urlImg1, urlImg2, urlImg3;
    private void cargarFotos(){
        try {
            img1 = (ImageView)findViewById(R.id.imageView);
            img2 = (ImageView)findViewById(R.id.imageView2);
            img3 = (ImageView)findViewById(R.id.imageView3);

            if(((String) getIntent().getStringExtra("imagen"))!=null && (!"".equals((String) getIntent().getStringExtra("imagen")))&& (!"sin_imagen".equals((String) getIntent().getStringExtra("imagen")))){
                urlImg1 = (String) getIntent().getStringExtra("imagen");
                if(urlImg1.contains("http:")) {
                    URL onLineURL = new URL(urlImg1);
                    new MyNetworkTask(img1).execute(onLineURL);

                }else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger
                    // images
                    options.inSampleSize = 8;

                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg1)); //BitmapFactory.decodeFile(urlImg1, options);
                    int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                    img1.setImageBitmap(scaled);
                }
            }else{
                img1.setImageResource(R.drawable.add_photo_icon);
            }

            if(((String) getIntent().getStringExtra("imagen2"))!=null && (!"".equals((String) getIntent().getStringExtra("imagen2")))&& (!"sin_imagen".equals((String) getIntent().getStringExtra("imagen2")))){
                urlImg2 = (String) getIntent().getStringExtra("imagen2");
                if(urlImg2.contains("http:")) {
                    URL onLineURL = new URL(urlImg2);
                    new MyNetworkTask(img2).execute(onLineURL);
                }else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger
                    // images
                    options.inSampleSize = 8;

                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg2));
                    int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                    img2.setImageBitmap(scaled);
                }
            }else{
                img2.setImageResource(R.drawable.add_photo_icon);
            }

            if(((String) getIntent().getStringExtra("imagen3"))!=null && (!"".equals((String) getIntent().getStringExtra("imagen3")))&& (!"sin_imagen".equals((String) getIntent().getStringExtra("imagen3")))){
                urlImg3 = (String) getIntent().getStringExtra("imagen3");
                if(urlImg3.contains("http:")) {
                    URL onLineURL = new URL(urlImg3);
                    new MyNetworkTask(img3).execute(onLineURL);

                }else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger
                    // images
                    options.inSampleSize = 8;

                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(urlImg3));
                    int nh = (int) ( bitmap.getHeight() * (800.0 / bitmap.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, nh, true);

                    img3.setImageBitmap(scaled);
                }


            }else{
                img3.setImageResource(R.drawable.add_photo_icon);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }


    private class MyNetworkTask extends AsyncTask<URL, Void, Bitmap> {

        ImageView tIV;

        public MyNetworkTask(ImageView iv){
            tIV = iv;
        }

        @Override
        protected Bitmap doInBackground(URL... urls) {

            Bitmap networkBitmap = null;

            URL networkUrl = urls[0]; //Load the first element
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inSampleSize = 2;
                // networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream());
                networkBitmap = BitmapFactory.decodeStream(networkUrl.openConnection().getInputStream(), null, options);


            } catch (IOException e) {
                e.printStackTrace();
            }

            return networkBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            tIV.setImageBitmap(result);
        }

    }

    class ImageCompressionAsyncTask extends AsyncTask<String, Void, String>{
        private boolean fromGallery;

        public ImageCompressionAsyncTask(boolean fromGallery){
            this.fromGallery = fromGallery;
        }

        @Override
        protected String doInBackground(String... params) {
            String filePath = compressImage(params[0]);
            return filePath;
        }

        public String compressImage(String imageUri) {

            String filePath = imageUri;
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            Bitmap bmp = BitmapFactory.decodeFile(filePath,options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16*1024];

            try{
                bmp = BitmapFactory.decodeFile(filePath,options);
            }
            catch(OutOfMemoryError exception){
                exception.printStackTrace();
            }

            try{
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            }
            catch(OutOfMemoryError exception){
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float)options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            String filename = getFilename();
            try {
                out = new FileOutputStream(filename);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return filename;

        }


        public String getFilename() {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "zonaduenio");

            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/"+ System.currentTimeMillis() + ".jpg");
            return uriSting;

        }



       /* @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
                 Bundle bundle = new Bundle();
                bundle.putString("FILE_PATH", result);
                showDialog(1, bundle);

        }*/

    }



    private void loadPaises(int posEdit){
        ArrayAdapter<PaisSpinner> dataAdapter = new ArrayAdapter<PaisSpinner>(this,
                android.R.layout.simple_spinner_item, paises);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerPaises.setAdapter(dataAdapter);
        if(posEdit!=0){
            spinnerPaises.setSelection(posEdit);
        }
    }

    private int getPositionPais(ArrayList<PaisSpinner> lista, int id){
        int pos = 0;
        for(PaisSpinner ps : lista){
            if (ps.tag.equals(id)){
                break;
            }
            pos++;
        }
        return pos;
    }

    Spinner spinnerProvincias;
    int intentosErrorProvincias = 0;
    ArrayList<ProvinciaSpinner> provincias = new ArrayList<ProvinciaSpinner>();
    private void cargarProvincias(final int idPais, boolean forzarRecarga){
        if(provincias==null || provincias.isEmpty() || provincias.size()<2 || forzarRecarga) {

            //List<ProvinciaSpinner> provinciasFiltradas = ProvinciasProvider.provincias.stream()
              //      .filter(provincia -> provincia.paisId == idPais).collect(Collectors.toList());

            List<ProvinciaSpinner> provinciasFiltradas = new ArrayList<ProvinciaSpinner>();
            provincias = new ArrayList<ProvinciaSpinner>();

            for(ProvinciaSpinner provincia : ProvinciasProvider.provincias){
                if(provincia.paisId == idPais) {
                    provinciasFiltradas.add(provincia);
                }
            }

            if (provinciasFiltradas.size() > 0) {
                int index = 0;
                int posEdit = 0;
                for (ProvinciaSpinner provincia : provinciasFiltradas) {
                    provincias.add(provincia);
                    if (idProvinciaEdit == (Integer) provincia.tag) {
                        posEdit = index;
                    }
                    index++;
                }
                loadProvincias(posEdit);
            } else {
                provincias.add(new ProvinciaSpinner("Sin datos", -1, 0));
                loadProvincias(0);
                localidades = new ArrayList<LocalidadSpinner>();
                localidades.add(new LocalidadSpinner("Sin datos", -1, 0, 0));
                loadLocalidades(0);
            }
        }else{
            int posEdit = 0;
            int i = 0;
            for (ProvinciaSpinner ps : provincias) {
                if (idProvinciaEdit == (int)ps.tag) {
                    posEdit = i;
                }
                i++;

            }
            loadProvincias(posEdit);
        }


    }

    private void loadProvincias(int posEdit){
        ArrayAdapter<ProvinciaSpinner> dataAdapter = new ArrayAdapter<ProvinciaSpinner>(this,
                android.R.layout.simple_spinner_item, provincias);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProvincias.setAdapter(dataAdapter);
        if(posEdit!=0){
            spinnerProvincias.setSelection(posEdit);
        }
    }


    Spinner spinnerLocalidades;
    ArrayList<LocalidadSpinner> localidades = new ArrayList<LocalidadSpinner>();
    int intentosErrorLocalidades = 0;
    private void cargarLocalidades(int idProvincia, boolean forzarRecarga){
        if(localidades==null || localidades.isEmpty() || localidades.size()<2 || forzarRecarga) {
            /*
            List<LocalidadSpinner> localidadesFiltradas = LocalidadesProvider.localidades.stream()
                    .filter(localidad -> localidad.provinciaId == idProvincia).collect(Collectors.toList());
            */
            List<LocalidadSpinner> localidadesFiltradas = new ArrayList<LocalidadSpinner>();
            localidades = new ArrayList<LocalidadSpinner>();

            for(LocalidadSpinner localidad : LocalidadesProvider.localidades){
                if(localidad.provinciaId == idProvincia) {
                    localidadesFiltradas.add(localidad);
                }
            }

            if (localidadesFiltradas.size() > 0) {
                int index = 0;
                int posEdit = 0;
                for (LocalidadSpinner localidad : localidadesFiltradas) {
                    localidades.add(localidad);
                    if (idLocalidadEdit == (Integer) localidad.tag) {
                        posEdit = index;
                    }
                    index++;
                }
                loadLocalidades(posEdit);
                findViewById(R.id.viewFlipper).setEnabled(true);
                findViewById(R.id.boton_modificar_guardar).setEnabled(true);
            } else {
                localidades.add(new LocalidadSpinner("Sin datos", -1, 0, 0));
                loadLocalidades(0);
            }

        }else{
            int posEdit = 0;
            int i = 0;
            for (LocalidadSpinner ps : localidades) {
                if (idLocalidadEdit == (int)ps.tag) {
                    posEdit = i;
                }
                i++;

            }
            loadLocalidades(posEdit);
            findViewById(R.id.viewFlipper).setEnabled(true);
            findViewById(R.id.boton_modificar_guardar).setEnabled(true);

        }

    }

    private void loadLocalidades(int posEdit){
        ArrayAdapter<LocalidadSpinner> dataAdapterLocalidades = new ArrayAdapter<LocalidadSpinner>(this,
                android.R.layout.simple_spinner_item, localidades);
        dataAdapterLocalidades.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocalidades.setAdapter(dataAdapterLocalidades);
        if(posEdit!=0){
            spinnerLocalidades.setSelection(posEdit);
        }
    }



    Spinner spinnerTipoPropiedad;
    ArrayList<TipoPropiedadSpinner> tipoPropiedades = new ArrayList<TipoPropiedadSpinner>();
    int intentosErrorTiposPropiedad = 0;
    private void cargarTipoPropiedad(){
        if(tipoPropiedades==null || tipoPropiedades.isEmpty()) {
            tipoPropiedades = new ArrayList<TipoPropiedadSpinner>();
            int index = 0;
            int posEdit = 0;
            for (TipoPropiedadSpinner tipoPropiedad : TipoPropiedadesProvider.tipoPropiedades) {
                tipoPropiedades.add(tipoPropiedad);
                if (tipoPropiedadEdit == (Integer) tipoPropiedad.tag) {
                    posEdit = index;
                }
                index++;
            }
            loadTipoPropiedad(posEdit);
        }else{
            int posEdit = 0;
            int i = 0;
            for (TipoPropiedadSpinner ps : tipoPropiedades) {
                if (tipoPropiedadEdit == (int)ps.tag) {
                    posEdit = i;
                }
                i++;

            }
            loadTipoPropiedad(posEdit);
        }
    }

    private void loadTipoPropiedad(int posEdit){
        ArrayAdapter<TipoPropiedadSpinner> dataAdapterTipoPropiedad = new ArrayAdapter<TipoPropiedadSpinner>(this,
                android.R.layout.simple_spinner_item, tipoPropiedades);
        dataAdapterTipoPropiedad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoPropiedad.setAdapter(dataAdapterTipoPropiedad);
        if(posEdit!=0){
            spinnerTipoPropiedad.setSelection(posEdit);
        }
    }

    //INICIO FTP


    public class ImageLoadingUtils {
        private Context context;
        public Bitmap icon;

        public ImageLoadingUtils(Context context){
            this.context = context;
            icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
        }

        public int convertDipToPixels(float dips){
            Resources r = context.getResources();
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dips, r.getDisplayMetrics());
        }

        public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }

            return inSampleSize;
        }

        public Bitmap decodeBitmapFromPath(String filePath){
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            scaledBitmap = BitmapFactory.decodeFile(filePath,options);

            options.inSampleSize = calculateInSampleSize(options, convertDipToPixels(150), convertDipToPixels(200));
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;

            scaledBitmap = BitmapFactory.decodeFile(filePath, options);
            return scaledBitmap;
        }





    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

    //Fin FTP






